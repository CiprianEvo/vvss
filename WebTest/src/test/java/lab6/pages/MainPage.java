package lab6.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

public class MainPage extends PageObject {
    @FindBy(name="titlu")
    private WebElementFacade titluInput;
    @FindBy(name="referenti")
    private WebElementFacade autoriInput;
    @FindBy(name="anAparitie")
    private WebElementFacade anInput;
    @FindBy(name="cuvinteCheie")
    private WebElementFacade keywordsInput;
    @FindBy(id="addButtonFuckSerenity")
    private WebElementFacade addButton;
    @FindBy(id="T")
    private WebElementFacade addedElement;


    public void writeTitlu(String titlu) {
        titluInput.waitUntilVisible();
        titluInput.sendKeys(titlu);
    }

    public void writeAutori(String autori) {
        autoriInput.waitUntilVisible();
        autoriInput.sendKeys(autori);
    }

    public void writeAn(String an) {
        anInput.waitUntilVisible();
        anInput.sendKeys(an);
    }

    public void writeKeywords(String keywords) {
        keywordsInput.waitUntilVisible();
        keywordsInput.sendKeys(keywords);
    }

    public void add(){
        addButton.waitUntilVisible();
        addButton.click();
    }

    public void checkIfAdded(){
        addedElement.waitUntilVisible();
    }

    public void checkIfNotAdded() {
        try {
            addedElement.waitUntilVisible();
            assert false;
        } catch (Exception e) {
            assert true;
        }
    }
}
