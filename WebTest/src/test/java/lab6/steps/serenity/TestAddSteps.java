package lab6.steps.serenity;

import lab6.pages.MainPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.StepGroup;
import net.thucydides.core.steps.ScenarioSteps;

public class TestAddSteps extends ScenarioSteps {
    MainPage mainPage;

    @Step
    public void navigateTo(String url) {
        getDriver().get(url);
    }

    @StepGroup
    public void writeData() {
        mainPage.writeTitlu("T");
        mainPage.writeAutori("A");
        mainPage.writeAn("1900");
        mainPage.writeKeywords("K");
    }

    @Step
    public void pressAdd() {
        mainPage.add();
    }

    @Step
    public void checkIfAdded() {
        mainPage.checkIfAdded();
    }

    @Step
    public void checkIfNotAdded() {
        mainPage.checkIfNotAdded();
    }
}
