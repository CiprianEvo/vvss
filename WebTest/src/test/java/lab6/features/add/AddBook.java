package lab6.features.add;

import lab6.steps.serenity.TestAddSteps;
import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.ManagedPages;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.pages.Pages;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

@RunWith(SerenityRunner.class)
public class AddBook {
    @Managed(uniqueSession = true)
    public WebDriver webdriver;

    @ManagedPages(defaultUrl = "http://localhost:8080/list")
    public Pages pages;

    @Steps
    public TestAddSteps steps;

    @Test
    public void addBookTest() {
        steps.navigateTo(pages.getDefaultBaseUrl());
        steps.writeData();
        steps.pressAdd();
        steps.checkIfAdded();
    }

    @Test
    public void addBookInvalidTest() {
        steps.navigateTo(pages.getDefaultBaseUrl());
        steps.pressAdd();
        steps.checkIfNotAdded();
    }
}
