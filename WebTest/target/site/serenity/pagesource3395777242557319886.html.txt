<html lang="en"><head>
    <meta charset="UTF-8">
    <title>Main page</title>
</head>
<body>

<form method="post" action="/adauga">
    Titlu<input type="text" name="titlu" id="titlu"><br>
    Autori<input type="text" name="referenti" id="referenti"><br>
    Anul de aparitie<input type="text" name="anAparitie" id="anAparitie"><br>
    Cuvinte cheie<input type="text" name="cuvinteCheie" id="cuvinteCheie"><br>
    <button type="submit" id="add">Add</button>
</form>

<br><br>

<ul>
    <li>
        <div>Povesti</div>
        <div>[Mihai Eminescu, Ion Caragiale, Ion Creanga]</div>
        <div>1973</div>
        <div>[povesti, povestiri]</div>
    </li>
    <li>
        <div>Poezii</div>
        <div>[Sadoveanu]</div>
        <div>1973</div>
        <div>[poezii]</div>
    </li>
    <li>
        <div>Enigma Otiliei</div>
        <div>[George Calinescu]</div>
        <div>1948</div>
        <div>[enigma, otilia]</div>
    </li>
    <li>
        <div>Dale carnavalului</div>
        <div>[Caragiale Ion]</div>
        <div>1948</div>
        <div>[caragiale, carnaval]</div>
    </li>
    <li>
        <div>Intampinarea crailor</div>
        <div>[Mateiu Caragiale]</div>
        <div>1948</div>
        <div>[mateiu, crailor]</div>
    </li>
    <li>
        <div>Test</div>
        <div>[Calinescu, Tetica]</div>
        <div>1992</div>
        <div>[am, casa]</div>
    </li>

</ul>


</body></html>