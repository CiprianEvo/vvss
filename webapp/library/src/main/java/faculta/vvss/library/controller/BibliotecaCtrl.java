package faculta.vvss.library.controller;


import faculta.vvss.library.domain.Carte;
import faculta.vvss.library.service.CartiService;
import faculta.vvss.library.validator.CarteValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletResponse;
import java.util.List;

@Controller
@RequestMapping(path = "/")
public class BibliotecaCtrl {
    private final CartiService service;

    @Autowired
    public BibliotecaCtrl(CartiService service) {
        this.service = service;
    }

    @RequestMapping(method = RequestMethod.POST, path = "/adauga")
    public String adaugaCarte(Carte c) throws Exception{
        CarteValidator.validateCarte(c);
        service.adaugaCarte(c);

        return "redirect:/list";
    }

    public List<Carte> cautaCarte(String autor) throws Exception{
        CarteValidator.isStringOK(autor);
        return service.cautaCarte(autor);
    }

    @RequestMapping(method = RequestMethod.GET, path = "/list")
    public String getCarti(Model model) throws Exception{
        model.addAttribute("carti", service.getCarti());

        return "page";
    }

    public List<Carte> getCartiOrdonateDinAnul(String an) throws Exception{
        if(!CarteValidator.isNumber(an))
            throw new Exception("Nu e numar!");
        return service.getCartiOrdonateDinAnul(an);
    }

}
